unit uNetChoice;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, FileUtil, Forms, Controls, Graphics, Dialogs, StdCtrls,
  Buttons;

type

  { TfrmNetChoice }

  TfrmNetChoice = class(TForm)
    BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
    cbMode: TComboBox;
    cbNetMode: TComboBox;
    eOperCode: TEdit;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
  private
    { private declarations }
  public
    { public declarations }
  end;

var
  frmNetChoice: TfrmNetChoice;

implementation

{$R *.lfm}

end.

