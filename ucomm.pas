unit uComm;

{$mode objfpc}{$H+}

interface

uses
   Classes, SysUtils,SynaSer;
var PortIn,PortOut:TFileStream;
    PortName:string={$IFDEF MSWINDOWS} 'COM4:' {$ENDIF} {$IFDEF UNIX} '/dev/ttyUSB0' {$ENDIF};
//Разделяет текст по символу/ам, при этом игнорирует символы в кавычках
//SL должен быть инициализирован
procedure CommaParser(const S,Sep:String;SL:TStrings);

function DeQuote(const s:string):string;

function CopyToEnd(const s:string;From:integer):string;

function StrReplace(const S,C1,C2:String):string;
implementation

procedure CommaParser(const S, Sep: String; SL: TStrings);
var
  s1: String;
  LevelQ,LevelP,Skip: Integer;
  i: Integer;
begin
  SL.Clear;
  if length(Sep)>=length(S) then exit;
  s1:='';LevelQ:=0;LevelP:=0;Skip:=0;
  for i:=1 to length(s) do
  begin
    if Skip>0 then begin Dec(Skip);continue; end;
    if s[i]='"' then begin
      if LevelQ>0 then Dec(LevelQ) else Inc(LevelQ);
    end;
    if s[i]='(' then inc(LevelP);
    if s[i]=')' then dec(LevelP);
    if (LevelQ=0)and(LevelP=0)and(Copy(s,i,length(Sep))=Sep) then begin
      SL.Add(s1);
      s1:='';Skip:=Length(Sep)-1;
    end else s1:=s1+s[i];
  end;
  if s1<>'' then SL.Add(s1);
end;

function DeQuote(const s: string): string;
begin
  Result:=s;
  if length(s)<2 then exit;
  if (s[1]='"')and(s[length(s)]='"') then Result:=copy(s,2,length(s)-2);
end;

function CopyToEnd(const s: string; From: integer): string;
begin
  Result:=Copy(s,From, Length(S)-From+1);
end;

function StrReplace(const S, C1, C2: String): string;
var
  p: SizeInt;
begin
  Result:=S;
  repeat
    p:=Pos(C1,Result);
    if P>0 then Result:=copy(Result,1,P-1)+C2+CopyToEnd(Result,p+Length(C1));
  until p<=0;
end;


end.

