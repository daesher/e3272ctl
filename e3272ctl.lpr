program e3272ctl;

{$mode objfpc}{$H+}

uses

  Interfaces, // this includes the LCL widgetset
  Forms, main, uComm, laz_synapse, rutrans, uNetChoice
  { you can add units after this };

{$R *.res}

begin
  RequireDerivedFormResource := True;
  Application.Initialize;
  Application.CreateForm(TForm1, Form1);
  Application.CreateForm(TfrmNetChoice, frmNetChoice);
  Application.Run;
end.

